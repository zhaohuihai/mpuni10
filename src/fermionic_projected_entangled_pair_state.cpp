/*
 * fermionic_projected_entangled_pair_state.cpp
 *
 *  Created on: 2015-10-21
 *      Author: zhaohuihai
 */

#include <mkl.h>

#include "fermionic_projected_entangled_pair_state.h"

using namespace std ;

FermionicProjectedEntangledPairState::FermionicProjectedEntangledPairState()
{
	_length_x = 0 ;
	_length_y = 0 ;
	_nsites = _length_x * _length_y ;
	_siteDim = 0 ;
	_bondDim = 0 ;
	_dimEven = 0 ;
	_dimOdd = 0 ;

	_chi = 0 ;
	_tol = 10 ;
	_maxIt = 0 ;

	_numel   = 0 ;
	_weight = 0.0 ;
	_new_weight = 0.0 ;
}

FermionicProjectedEntangledPairState::FermionicProjectedEntangledPairState(
		const int length_x, const int length_y,
		const int siteDim,  const int bondDim, const int dimEven,
		double* elem, const int chi, const double tol, const int maxIt)
{
	_length_x = length_x ;
	_length_y = length_y ;
	_nsites = length_x * length_y ;
	_siteDim = siteDim ;
	_bondDim = bondDim ;
	_dimEven = dimEven ;
	_dimOdd = bondDim - dimEven ;

	_chi = chi ;
	_tol = tol ;
	_maxIt = maxIt ;

	_numel   = 0 ;
	_weight = 0.0 ;
	_new_weight = 0.0 ;

	set_ti() ;
	_siteElem.resize(_nsites) ;
	double* val = elem ;
	int nval = 0 ; // count the total number of variational parameters
	double* ti_val ;
	for ( int i = 0 ; i < _nsites; i ++ ) {
		if ( _ti[i] == false ) {
			for ( int j = 0; j < _siteDim; j ++ ) {
				_siteElem.at(i).push_back(val) ;
				int n = getSiteElemNum(_dimEven, _dimOdd, j) ;
				val += n ;
				nval += n ;
			}
		} else { // translational invariant site tensors
			for ( int j = 0; j < _siteDim; j ++ ) {
				ti_val = _siteElem[ _tiSite.at(i) ][j] ;
				_siteElem.at(i).push_back(ti_val) ;
			}
		}
	}
	_numel = nval ;
}

int FermionicProjectedEntangledPairState::getSiteElemNum(
		const int dimEven, const int dimOdd, const int physInd)
{
	uni10::Qnum qfe(uni10::PRTF_EVEN);
	uni10::Qnum qfo(uni10::PRTF_ODD);
  std::vector<uni10::Qnum> qnums;
  for (int i = 0; i < dimEven; i ++) qnums.push_back(qfe) ;
  for (int j = 0; j < dimOdd; j ++) qnums.push_back(qfo) ;

  std::vector<uni10::Qnum> qnPhys ;
  if ( physInd == 0 || physInd == 3 ) // even
  	qnPhys.push_back(qfe) ;
  else // odd
  	qnPhys.push_back(qfo) ;

  uni10::Bond bd_in(uni10::BD_IN, qnums);
  uni10::Bond bd_out(uni10::BD_OUT, qnums);
  uni10::Bond bd_phys(uni10::BD_OUT, qnPhys);
  std::vector<uni10::Bond> bonds;
	bonds.push_back(bd_in);
	bonds.push_back(bd_in);
	bonds.push_back(bd_out);
	bonds.push_back(bd_out);
	bonds.push_back(bd_phys);
	uni10::UniTensor T(bonds, "T");
	return T.elemNum() ;
}

uni10::UniTensor FermionicProjectedEntangledPairState::getSiteTensor(
		const int iLattice, const int physInd)
{
	uni10::Qnum qfe(uni10::PRTF_EVEN);
	uni10::Qnum qfo(uni10::PRTF_ODD);
  std::vector<uni10::Qnum> qnums;
  for (int i = 0; i < _dimEven; i ++) qnums.push_back(qfe) ;
  for (int j = 0; j < _dimOdd; j ++) qnums.push_back(qfo) ;

  std::vector<uni10::Qnum> qnPhys ;
  if ( physInd == 0 || physInd == 3 ) // even
  	qnPhys.push_back(qfe) ;
  else // odd
  	qnPhys.push_back(qfo) ;

  uni10::Bond bd_in(uni10::BD_IN, qnums);
  uni10::Bond bd_out(uni10::BD_OUT, qnums);
  uni10::Bond bd_phys(uni10::BD_OUT, qnPhys);
  std::vector<uni10::Bond> bonds;
	bonds.push_back(bd_in);
	bonds.push_back(bd_in);
	bonds.push_back(bd_out);
	bonds.push_back(bd_out);
	bonds.push_back(bd_phys);
	uni10::UniTensor T(bonds, "T");
	double * val = _siteElem.at(iLattice).at(physInd) ;
	T.setElem(val) ;
	return T ;
}

void FermionicProjectedEntangledPairState::initMakeSample(const int *eleNum)
{
	int nsites2 = _nsites * 2 ;
	_eleNum = std::vector< int >(nsites2, -1) ;
	for (int i = 0; i < nsites2; i ++)
	{
		_eleNum.at(i) = eleNum[i] ;
	}
	_weight = compute_weight(eleNum) ;
}

void FermionicProjectedEntangledPairState::initPhysCal(const int *eleNum)
{
	if ( _bondDim == 0 ) return ; // no PEPS in the wave function

	_rowMPOs.resize(_length_y) ;
	// row MPOs
	for ( int j = 0; j < _length_y; j ++ ) {
		computeRowMPO(eleNum, j, _rowMPOs.at(j)) ;
	}
	_colMPOs.resize(_length_x) ;
	// column MPOs
	for ( int i = 0; i < _length_x; i ++ ) {
//		std::cout << "column number i: " << i << std::endl ;
		computeColMPO(eleNum, i, _colMPOs.at(i) ) ;
	}
//	exit(0) ;
	//============================================================
	int nsites2 = _nsites * 2 ;
	_eleNum = std::vector< int >(nsites2, -1) ;
	for (int i = 0; i < nsites2; i ++)
	{
		_eleNum.at(i) = eleNum[i] ;
	}
	_weight = compute_weight(eleNum) ;
}

void FermionicProjectedEntangledPairState::update()
{
	_eleNum = _new_eleNum ;
	_weight = _new_weight ;
}

double FermionicProjectedEntangledPairState::compute_weight(const int* eleNum)
{
//	StartTimer(102) ;
	if ( _bondDim == 0 ) return 1.0 ; // no PEPS in the wave function
	double weight ;

	std::vector< uni10::UniTensor > mpo ;
	mpo.resize(_length_x) ;
	int physInd ;
	for ( int i = 0; i < _length_x; i ++ ) // mpo of first row
	{
		physInd = getPhysInd(eleNum, i) ;
		mpo[i] = getSiteTensor(i, physInd) ;
//		std::cout << "site: " << i << ", physInd: " << physInd << std::endl ;
//		cout << mpo[i] << endl ;
	}
	int iLattice ;

	std::vector< uni10::UniTensor > mpoAdd ; // the row below mpo
	mpoAdd.resize(_length_x) ;
	for ( int j = 1; j < _length_y; j ++ ) // row index
	{
		for ( int i = 0; i < _length_x; i ++ ) // column index
		{
			iLattice = i + j * _length_x ; // lattice site index
			physInd = getPhysInd(eleNum, iLattice) ;
			mpoAdd[i] = getSiteTensor(iLattice, physInd) ;
//			std::cout << "site: " << iLattice << ", physInd: " << physInd << std::endl ;
//			cout << mpoAdd[i] << endl ;
		}
		if ( j != (_length_y - 1) ) {
//			std::cout << "mpoAdd row: " << j << std::endl ;
			updateMPO(mpo, mpoAdd, mpo) ;
		}
		else {
//			std::cout << "mpoAdd row: " << j << ", last row." << std::endl ;
			weight = contract2RowMPOs(mpo, mpoAdd) ;
		}
	}
//	StopTimer(102) ;
	return weight ;
}

double FermionicProjectedEntangledPairState::compute_weight(const std::vector<int>& eleNum)
{
	return compute_weight(&eleNum.at(0)) ;
}

// An electron with spin s hops from ri to rj.
double FermionicProjectedEntangledPairState::computeRatio(const int ri, const int rj, const int s)
{
	if ( _bondDim == 0 ) return 1.0 ; // no PEPS in the wave function
	// ri = ix + iy * _length_x ;
	int ix = ri % _length_x ;
	int iy = ri / _length_x ;
//	std::cout << "site: " << ri << ", coordinate: " << ix << ", " << iy << std::endl ;
	int jx = rj % _length_x ;
	int jy = rj / _length_x ;
//	std::cout << "site: " << rj << ", coordinate: " << jx << ", " << jy << std::endl ;

	_new_eleNum = _eleNum ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	_new_eleNum.at(rsi) = 0 ;
	_new_eleNum.at(rsj) = 1 ;
	std::vector< uni10::UniTensor > mpoAdd ;
//	int iLattice, physInd ;
	_new_weight = 1.0 ;
	if ( iy == jy ) // ri and rj are in the same row
	{
		mpoAdd.resize(_length_x) ;
		for ( int i = 0; i < _length_x; i ++ ) {
			int iLattice = i + iy * _length_x ;
			int physInd = getPhysInd(_new_eleNum, iLattice) ;
			mpoAdd[i] = getSiteTensor(iLattice, physInd) ;
		}
		_new_weight = contract2RowMPOs(mpoAdd, _rowMPOs.at(iy) ) ;
	}
	else if (ix == jx) // ri and rj are in the same column
	{
		mpoAdd.resize(_length_y) ;
		for ( int j = 0; j < _length_y; j ++ ) {
			int iLattice = ix + j * _length_x ;
			int physInd = getPhysInd(_new_eleNum, iLattice) ;
			mpoAdd[j] = getSiteTensor(iLattice, physInd) ;
		}
		_new_weight = contract2ColMPOs(mpoAdd, _colMPOs.at(ix) ) ;
	}
	else // ri and rj are neither in same row nor in same column
	{
		_new_weight = compute_weight(_new_eleNum) ;
	}

//	double new_weight = compute_weight(_new_eleNum) ;
//	std::cout << "old weight in computeRatio: " << _weight << std::endl ;
//	std::cout << "weight diff: " << (_new_weight - new_weight) << std::endl ;
	return (_new_weight / _weight) ;
}

// An electron with spin s hops from ri to rj.
// An electron with spin t hops from rk to rl.
double FermionicProjectedEntangledPairState::computeRatio(const int ri, const int rj, const int s,
																												  const int rk, const int rl, const int t)
{
	if ( _bondDim == 0 ) return 1.0 ; // no PEPS in the wave function
	_new_eleNum = _eleNum ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	int rtk = rk + t * _nsites ;
	int rtl = rl + t * _nsites ;
	_new_eleNum.at(rsi) = 0 ;
	_new_eleNum.at(rsj) = 1 ;
	_new_eleNum.at(rtk) = 0 ;
	_new_eleNum.at(rtl) = 1 ;

	_new_weight = compute_weight(_new_eleNum) ;
	return (_new_weight / _weight) ;
}

// compute log(<x'|PEPS> / <x|PEPS>)
double FermionicProjectedEntangledPairState::computeLogRatio(const int ri, const int rj, const int s)
{
	if ( _bondDim == 0 ) return 0.0 ; // no PEPS in the wave function
	_new_eleNum = _eleNum ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	_new_eleNum.at(rsi) = 0 ;
	_new_eleNum.at(rsj) = 1 ;

	_new_weight = compute_weight(_new_eleNum) ;
	double logRatio = std::log(std::fabs( _new_weight / _weight )) ;
	return logRatio ;
}

// An electron with spin s hops from ri to rj.
// An electron with spin t hops from rk to rl.
double FermionicProjectedEntangledPairState::computeLogRatio(const int ri, const int rj, const int s,
																														 const int rk, const int rl, const int t)
{
	if ( _bondDim == 0 ) return 0.0 ; // no PEPS in the wave function
	_new_eleNum = _eleNum ;
	int rsi = ri + s * _nsites ;
	int rsj = rj + s * _nsites ;
	int rtk = rk + t * _nsites ;
	int rtl = rl + t * _nsites ;
	_new_eleNum.at(rsi) = 0 ;
	_new_eleNum.at(rsj) = 1 ;
	_new_eleNum.at(rtk) = 0 ;
	_new_eleNum.at(rtl) = 1 ;

	_new_weight = compute_weight(_new_eleNum) ;
	double logRatio = std::log(std::fabs( _new_weight / _weight )) ;
	return logRatio ;
}

void FermionicProjectedEntangledPairState::derivative(double *der, const int* eleNum)
{
//	StartTimer(103) ;
	if ( _bondDim == 0 ) return ;
	// initialize all derivative value to 0.0
	for (int i = 0; i < _numel; i ++) der[i] = 0.0 ;

//	double weight1 = compute_weight(eleNum) ;
//	std::cout << "_weight diff: " << ( weight1 - _weight) << std::endl ;

	int physInd ;
	double* siteDerPtr ;
	for ( int i = 0; i < _nsites; i ++ ) {
		physInd = getPhysInd(eleNum, i) ;
		siteDerPtr = getSitePtr(der, i, physInd) ;
		siteDerivative(eleNum, i, siteDerPtr) ;
	}
//	StopTimer(103) ;
}

double* FermionicProjectedEntangledPairState::getSitePtr(double *der, int i, int physInd)
{
	int offset = _siteElem.at(i).at(physInd) - _siteElem.at(0).at(0) ;
	double* siteDerPtr ;

	siteDerPtr = der + offset ;
//	std::cout << "site: " << i << ", physInd: " << physInd << ", offset: " << offset << std::endl ;
	return siteDerPtr ;
}

void FermionicProjectedEntangledPairState::siteDerivative(const int* eleNum,
																												  int r, double* siteDer)
{
	// r = ix + iy * _length_x ;
	int ix = r % _length_x ;
	int iy = r / _length_x ;

	std::vector< uni10::UniTensor > mpoDer ;
	mpoDer.resize(_length_x) ; // row with site r
	int physInd, iLattice ;
	for ( int i = 0; i < _length_x; i ++ ) {
		iLattice = i + iy * _length_x ;
		physInd = getPhysInd(eleNum, iLattice) ;
		mpoDer[i] = getSiteTensor(iLattice, physInd) ;
	}

	int envSiteNo = ( ix + 1 )% _length_x ;
	uni10::UniTensor A, B ;
	int label_A[] = {1, 2, 3, 4, 101} ;
	int label_B[] = {3, 4, 5, 6, 102} ;
	vector<int> label_com;
	label_com.push_back(101) ;
	label_com.push_back(102) ;
	verticalTrace(mpoDer[envSiteNo], _rowMPOs.at(iy).at(envSiteNo), A) ;
	// multiply matrices from (ix+1)%_length_x to (ix-1)%_length_x
	for ( int i = 2; i < _length_x; i ++ )
	{
		envSiteNo = (ix + i) % _length_x ;
		verticalTrace(mpoDer[envSiteNo], _rowMPOs.at(iy).at(envSiteNo), B) ;
		A.setLabel(label_A) ; // A(1,2,3,4,101)
		B.setLabel(label_B) ; // B(3,4,5,6,102)
		A = contract(A, B, false) ; // A(1,2,101,5,6,102)
		A.combineBond(label_com) ; // A(1,2,101,5,6)
		int label_Anew[] = {1,2,5,6,101} ;
		A = A.permute(label_Anew, 2) ;
	}
	A.setLabel(label_A) ; // A(1,2,3,4,101)
	B = _rowMPOs.at(iy).at(ix) ; // B(x4,x2,y2,y1,m2)
	label_B[0] = 4; label_B[1] = 2; label_B[2] = 12; label_B[3] = 11; label_B[4] = 102 ;
	B.setLabel(label_B) ; // B(4, 2, 12, 11,102)
	// A(x1,x3,m1,y2,y1,m2) = sum{x2,x4}_[A(x1,x2,x3,x4,m1) * B(x4,x2,y2,y1,m2)]
	A = contract(A, B, false) ; // A(1,3,101,12,11,102)
	A.combineBond(label_com) ; // (1,3,101,12,11)
	int label_Anew[] = {3, 1, 11, 12,101} ;
	// A(x1,x3,m1,y2,y1) -> A(x3,x1,y1,y2,m1)
	A = A.permute(label_Anew, 2) ;

	int n = A.elemNum() ;
	//-----------------------------------
//	std::cout << "ix:" << ix << std::endl ;
//	std::cout << "weight diff:" << (dot(A, mpoDer[ix]) - _weight)/_weight << std::endl ;
	//-----------------------------------
	for ( int j = 0; j < n; j ++ ) {
//		std::cout << siteDer[j] << ", "  ;
		siteDer[j] += A[j] / _weight ;
//		std::cout << siteDer[j] << std::endl ;
	}
}

void FermionicProjectedEntangledPairState::updateMPO(vector< uni10::UniTensor >& upMPO,
		vector< uni10::UniTensor >& downMPO, vector< uni10::UniTensor >& udMPO)
{
	int dx1 = upMPO[0].bond()[0].dim() ;
	int dx2 = downMPO[0].bond()[0].dim() ;
	if ( (dx1*dx2) <= _chi ) { // no truncation
		for ( int i = 0; i < _length_x; i ++ ) {
			verticalContract(upMPO[i], downMPO[i], udMPO[i]) ;
		}
	} else {
		canonicalize(upMPO, downMPO, udMPO) ;
	}
}

void FermionicProjectedEntangledPairState::canonicalize(vector< uni10::UniTensor >& upMPO,
																											  vector< uni10::UniTensor >& downMPO,
																											  vector< uni10::UniTensor >& udMPO)
{
	cout << "FermionicProjectedEntangledPairState::canonicalize not implemented." << endl ;
	exit(0) ;
}

// input: T1, T2, T3, T4
// output: PRA, PLB, S, truncErr
void FermionicProjectedEntangledPairState::getGauge(uni10::UniTensor& T1, uni10::UniTensor& T2,
		uni10::UniTensor& T3, uni10::UniTensor& T4,
		uni10::UniTensor& PRA, uni10::UniTensor& PLB,
		uni10::UniTensor& S, double& truncErr)
{
	cout << "FermionicProjectedEntangledPairState::getGauge not implemented." << endl ;
	exit(0) ;
}

void FermionicProjectedEntangledPairState::updateColMPO(std::vector< uni10::UniTensor >& lMPO,
																											  std::vector< uni10::UniTensor >& rMPO,
																											  std::vector< uni10::UniTensor >& lrMPO)
{
	int dy1 = lMPO[0].bond()[2].dim() ;
	int dy2 = rMPO[0].bond()[2].dim() ;
	if ( ( dy1 * dy2 ) <= _chi ) {
		for ( int j = 0; j < _length_y; j ++ ) {
			horizontalContract(lMPO[j], rMPO[j], lrMPO[j]) ;
		}
	} else canonicalizeCol(lMPO, rMPO, lrMPO) ;
}

void FermionicProjectedEntangledPairState::canonicalizeCol(std::vector<uni10::UniTensor > lMPO,
																											     std::vector<uni10::UniTensor > rMPO,
																													 std::vector<uni10::UniTensor >& lrMPO)
{
	cout << "FermionicProjectedEntangledPairState::canonicalizeCol not implemented." << endl ;
	exit(0) ;
}

// contract 2 vertical sites tensors directly without truncation
void FermionicProjectedEntangledPairState::verticalContract(uni10::UniTensor& T1,
		uni10::UniTensor& T2, uni10::UniTensor& newT)
{
	// T1(x1,x3,y1,y2,m1)  T2(x2,x4,y2,y3,m2)
	//   (1, 3, 11,12,101)    (2 ,4,12,13,102)
	int label_T1[] = {1, 3, 11, 12, 101} ;
	int label_T2[] = {2, 4, 12, 13, 102} ;
	T1.setLabel(label_T1) ;
	T2.setLabel(label_T2) ;
	newT = contract(T1, T2, false) ; // (1,3,11,101,2,4,13,102)
//	cout << newT << endl ;
	vector<int> label_com ;
	label_com.push_back(1) ;
	label_com.push_back(2) ;
	newT.combineBond(label_com) ; // (1,3,11,101,4,13,102)
//	cout << newT << endl ;
	label_com[0] = 3; label_com[1] = 4;
	newT.combineBond(label_com) ; // (1,3,11,101,13,102)
//	cout << newT << endl ;
	label_com[0] = 101; label_com[1] = 102;
	newT.combineBond(label_com) ; // (1,3,11,101,13)
//	cout << newT << endl ;
	int label_new[] = {1,3,11,13,101} ;
	newT = newT.permute(label_new, 2) ;
}

void FermionicProjectedEntangledPairState::horizontalContract(uni10::UniTensor& T1,
		uni10::UniTensor& T2, uni10::UniTensor& newT)
{
	// T1(x1,x2,y1,y3,m1) T2(x2,x3,y2,y4,m2)
	//   (1, 2, 11,13,101)  (2,  3,12,14,102)
	int label_T1[] = {1, 2, 11, 13, 101} ;
	int label_T2[] = {2, 3, 12, 14, 102} ;
	T1.setLabel(label_T1) ;
	T2.setLabel(label_T2) ;
	newT = contract(T1, T2, false) ; // (1,11,13,101,3,12,14,102)
	vector<int> label_com ;
	label_com.push_back(11) ;
	label_com.push_back(12) ;
	newT.combineBond(label_com) ; // (1,11,13,101,3,14,102)
	label_com[0] = 13; label_com[1] = 14;
	newT.combineBond(label_com) ; // (1,11,13,101,3,102)
	label_com[0] = 101; label_com[1] = 102;
	newT.combineBond(label_com) ; // (1,11,13,101,3)

	int label_new[] = {1,3,11,13,101} ;
	newT = newT.permute(label_new, 2) ;
}

double FermionicProjectedEntangledPairState::contract2RowMPOs(
		std::vector< uni10::UniTensor >& upMPO, std::vector< uni10::UniTensor >& downMPO)
{
	uni10::UniTensor A, B;
	int label_A[] = {1, 2, 3, 4, 101} ;
	int label_B[] = {3, 4, 5, 6, 102} ;
	vector<int> label_com;
	label_com.push_back(101) ;
	label_com.push_back(102) ;
	verticalTrace(upMPO[0], downMPO[0], A) ; // A(1,2,3,4,101)
//	cout << A << endl ;
	for ( int i = 1; i < (_length_x - 1 ); i ++ ) {
		verticalTrace(upMPO[i], downMPO[i], B) ; // B(3,4,5,6,102)
		A.setLabel(label_A) ;
		B.setLabel(label_B) ;
		A = contract(A, B, false) ; // A(1,2,101,5,6,102)
		A.combineBond(label_com) ; // A(1,2,101,5,6)
		int label_Anew[] = {1,2,5,6,101} ;
		A = A.permute(label_Anew, 2) ;
	}
	A.setLabel(label_A) ; // A(1,2,3,4,101)
	verticalTrace(upMPO[_length_x - 1], downMPO[_length_x - 1], B) ;
	label_B[2] = 1; label_B[3] = 2;
	B.setLabel(label_B) ; // B(3,4,1,2,102)

	A = contract(A, B, false) ; // A(101,102)
//	cout << A << endl ;
	double weight = A[0] ;
//	std::cout << "weight: "<< weight << std::endl ;
	return weight ;
}

double FermionicProjectedEntangledPairState::contract2ColMPOs(
		std::vector< uni10::UniTensor >& lMPO, std::vector< uni10::UniTensor >& rMPO)
{
	uni10::UniTensor A, B;
	int label_A[] = {11, 12, 13, 14, 101} ;
	int label_B[] = {13, 14, 15, 16, 102} ;
	vector<int> label_com;
	label_com.push_back(101) ;
	label_com.push_back(102) ;
	horizontalTrace(lMPO[0], rMPO[0], A) ; // A(11, 12, 13, 14, 101)
	for ( int j = 1; j < (_length_y - 1); j ++ ) {
		horizontalTrace(lMPO[j], rMPO[j], B) ; // B(13, 14, 15, 16, 102)
		A.setLabel(label_A) ;
		B.setLabel(label_B) ;
		A = contract(A, B, false) ; // A(11,12,101,15,16,102)
		A.combineBond(label_com) ; // A(11,12,101,15,16)
		int label_Anew[] = {11,12,15,16,101} ;
		A = A.permute(label_Anew, 2) ;
	}
	A.setLabel(label_A) ; // A(11, 12, 13, 14, 101)
	horizontalTrace(lMPO[_length_y - 1], rMPO[_length_y - 1], B) ;
	label_B[2] = 11; label_B[3] = 12;
	B.setLabel(label_B) ; // B(13,14,11,12,102)

	A = contract(A, B, false) ; // A(101,102)
	double weight = A[0] ;
	return weight ;
}

void FermionicProjectedEntangledPairState::verticalTrace(
		uni10::UniTensor& T1, uni10::UniTensor& T2, uni10::UniTensor& M)
{
	// T1(x1,x3,y1,y2,m1)  T2(x2,x4,y2,y1,m2)
	//   (1, 3, 11,12,101)    (2 ,4,12,11,102)
	int label_T1[] = {1, 3, 11, 12, 101} ;
	int label_T2[] = {2, 4, 12, 11, 102} ;
	T1.setLabel(label_T1) ;
	T2.setLabel(label_T2) ;
	M = contract(T1, T2, false) ; // (1,3,101,2,4,102)

//	cout << M << endl ;
	vector<int> label_com;
	label_com.push_back(101) ;
	label_com.push_back(102) ;
	M.combineBond(label_com) ; // (1,3,101,2,4)
//	cout << M << endl ;
	int label_Anew[] = {1, 2, 3, 4,101} ;
	M.permute(label_Anew, 2) ; // (1, 2, 3, 4,101)
//	cout << M << endl ;
}

void FermionicProjectedEntangledPairState::horizontalTrace(
		uni10::UniTensor& T1, uni10::UniTensor& T2, uni10::UniTensor& M)
{
	// T1(x1,x2,y1,y3,m1) T2(x2,x1,y2,y4,m2)
	//   (1, 2, 11,13,101)  (2,  1,12,14,102)
	int label_T1[] = {1, 2, 11,13,101} ;
	int label_T2[] = {2, 1, 12,14,102} ;
	T1.setLabel(label_T1) ;
	T2.setLabel(label_T2) ;
	M = contract(T1, T2, false) ; // (11,13,101,12,14,102)

	vector<int> label_com;
	label_com.push_back(101) ;
	label_com.push_back(102) ;
	M.combineBond(label_com) ; // (11,13,101,12,14)

	int label_Anew[] = {11, 12, 13, 14, 101} ;
	M.permute(label_Anew, 2) ;
}

// contract lattice except row j to form an MPO
void FermionicProjectedEntangledPairState::computeRowMPO(const int *eleNum, int j,
																					std::vector< uni10::UniTensor >& rowMPO)
{
	rowMPO.resize(_length_x) ;
	int iLattice, physInd ;
	int jRow = (j + 1) % _length_y ;
	for ( int i = 0; i < _length_x; i ++ ) {
		iLattice = i + jRow * _length_x ;
		physInd = getPhysInd(eleNum, iLattice) ;
		rowMPO.at(i) = getSiteTensor(iLattice, physInd) ;
	}

	std::vector< uni10::UniTensor > mpoAdd ;
	mpoAdd.resize(_length_x) ;
	for ( jRow = (j+2)%_length_y; jRow != j; jRow = (jRow + 1)%_length_y ) {
		for ( int i = 0; i < _length_x; i ++ ) // column index
		{
			iLattice = i + jRow * _length_x ; // lattice site index
			physInd = getPhysInd(eleNum, iLattice) ;
			mpoAdd.at(i) = getSiteTensor(iLattice, physInd) ;
		}
		updateMPO(rowMPO, mpoAdd, rowMPO) ;
	}
}

// contract lattice except column i to form an MPO
void FermionicProjectedEntangledPairState::computeColMPO(const int *eleNum, int i,
																					std::vector< uni10::UniTensor >& colMPO)
{
	colMPO.resize(_length_y) ;
	int iLattice, physInd ;
	int iCol = (i + 1) % _length_x ;
//	std::cout << "iLattice: " ;
	for ( int j = 0; j < _length_y; j ++ ) {
		iLattice = iCol + j * _length_x ;
//		std::cout << iLattice << ", " ;
		physInd = getPhysInd(eleNum, iLattice) ;
		colMPO[j] = getSiteTensor(iLattice, physInd) ; ;
	}
//	std::cout << std::endl ;
	std::vector< uni10::UniTensor > mpoAdd ;
	mpoAdd.resize(_length_y) ;

	for ( iCol = (i + 2)%_length_x; iCol != i; iCol = (iCol + 1)%_length_x ) {
//		std::cout << "iCol: " << iCol << std::endl ;
//		std::cout << "iLattice: " ;
		for ( int j = 0; j < _length_y; j ++ ) // row index
		{
			iLattice = iCol + j * _length_x ;
//			std::cout << iLattice << ", " ;
			physInd = getPhysInd(eleNum, iLattice) ;
			mpoAdd[j] = getSiteTensor(iLattice, physInd) ; ;
		}
//		std::cout << std::endl ;
		updateColMPO(colMPO, mpoAdd, colMPO) ;
	}
}

void FermionicProjectedEntangledPairState::set_ti()
{
	_ti.assign(size_t(_nsites), false) ;
	_tiSite.assign(size_t(_nsites), -1) ;

	for ( int i = 2; i < _ti.size(); i ++ ) _ti[i] = true ;

	for ( int i = 2; i < _tiSite.size(); i ++ ) {
		int x = i % _length_x ;
		int y = i / _length_x ;
		if ( (x+y) % 2 == 0 ) _tiSite[i] = 0 ;
		else _tiSite[i] = 1 ;
	}
}

int FermionicProjectedEntangledPairState::getPhysInd(const int* eleNum, const int i)
{
	int physInd ;
	int upInd, downInd ;
	upInd = eleNum[i] ;
	downInd = eleNum[i + _nsites] ;
	physInd = upInd + downInd * 2 ;
	if ( physInd < 0 || physInd >= _siteDim)
	{
		std::cout << "ProjectedEntangledPairState<SCALAR>::getPhysInd error: " ;
		std::cout << "physInd = " << physInd << ", which is out of range." << std::endl ;
		exit(0) ;
	}
	return physInd ;
}

int FermionicProjectedEntangledPairState::getPhysInd(const std::vector<int>& eleNum, const int i)
{
	return getPhysInd(&eleNum.at(0), i) ;
}

// rescale every tensor with largest element normalized to maxElem
void FermionicProjectedEntangledPairState::rescale(double maxElem)
{
	int N = FPEPSsiteElemNum(_siteDim, _bondDim, _dimEven) ;
	for ( int i = 0; i < _nsites; i ++ ) {
		if ( _ti[i] == false ) {
//			cout << "elemNum: " << N << endl ;
			double* V = _siteElem.at(i).at(0) ;
			int incx = 1 ;
			int j = idamax(&N, V, &incx) ;
			double xmax = fabs(V[j - 1]) ;

			double ratio = maxElem / xmax ;
			for (int k = 0; k < N; k ++)
			{
//				cout << V[k] << ", " ;
				V[k] *= ratio ;
//				cout << V[k] << endl ;
			}
		}
	}
}

//==============================================================================
// non-member function

int FPEPSsiteElemNum(const int siteDim,  const int bondDim, const int dimEven)
{
	int dimOdd = bondDim - dimEven ;
	uni10::Qnum qfe(uni10::PRTF_EVEN);
	uni10::Qnum qfo(uni10::PRTF_ODD);
  std::vector<uni10::Qnum> qnums;
  for (int ie = 0; ie < dimEven; ie ++) qnums.push_back(qfe) ;
  for (int io = 0; io < dimOdd; io ++) qnums.push_back(qfo) ;
  std::vector<uni10::Qnum> qnPhys ;
  qnPhys.push_back(qfe) ; // 0
  qnPhys.push_back(qfo) ; // up
  qnPhys.push_back(qfo) ; // down
  qnPhys.push_back(qfe) ; // up,down

  uni10::Bond bd_in(uni10::BD_IN, qnums);
  uni10::Bond bd_out(uni10::BD_OUT, qnums);
  uni10::Bond bd_phys(uni10::BD_OUT, qnPhys);
  std::vector<uni10::Bond> bonds;
	bonds.push_back(bd_in);
	bonds.push_back(bd_in);
	bonds.push_back(bd_out);
	bonds.push_back(bd_out);
	bonds.push_back(bd_phys);
	uni10::UniTensor T(bonds, "T");

	return T.elemNum() ;
}
